﻿using UnityEngine;

namespace Vision.VisualizationModules
{
    public class ParticleVisualizationModule : VisualizationModule
    {
        [Header("General")]
        public ParticleSystem ParticleSystemComponent;

        [Header("Emission")]
        public bool UseStopPlay;
        public float PlayThreshold;

        [Header("StartColor")]
        public bool UseStartColor;
        public Gradient StartColorGradient;

        private ParticleSystem.MainModule _main;
        private ParticleSystem.EmissionModule _emission;

        private void Awake()
        {
            if (ParticleSystemComponent)
            {
                _main = ParticleSystemComponent.main;
                _emission = ParticleSystemComponent.emission;
            }
        }

        public override void OnUpdate()
        {
            if (ParticleSystemComponent == null)
                return;

            base.OnUpdate();

            if (UseStopPlay) 
            {
                _emission.enabled = _currentValue >= PlayThreshold;
            }

            if (UseStartColor)
            {
                var startColor = _main.startColor;
                startColor.color = StartColorGradient.Evaluate(_currentValue);

                _main.startColor = startColor;
            }
        }
    }
}