﻿using System.Collections.Generic;
using UnityEngine;

namespace Vision.VisualizationModules
{
    public class VisualizationModule : MonoBehaviour
    {
        [Header("Visualization Data")]
        public float ValueAttackVelocity = 1f;
        public float ValueReleaseVelocity = .2f;

        [Header("Value Type")]
        public bool CreateDeltaValue;
        public float DeltaFrameInSeconds = 1f;
        public AnimationCurve DeltaValueDistribution = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(1f, 1f) });


        internal float _currentValue = 0f;
        internal float _targetValue = 0f;
        internal float _deltaValue = 0f;

        private float _deltaCurrentFrame = 0f;
        //private float _lastValuesAverage = 0f;
        private List<float> _targetValueHistory = new List<float>();


        private void OnEnable()
        {
            _targetValueHistory.Clear();
        }

        private void Update()
        {
            if (CreateDeltaValue)
            {
                _targetValueHistory.Add(_currentValue);

                if (_deltaCurrentFrame < DeltaFrameInSeconds)
                    _deltaCurrentFrame += Time.deltaTime;
                else
                {
                    if (_targetValueHistory.Count > 0)
                    {
                        float minVal = _targetValueHistory[0];
                        float maxVal = _targetValueHistory[0];

                        for (int i = 1; i < _targetValueHistory.Count; i++)
                        {
                            if (_targetValueHistory[i] < minVal)
                                minVal = _targetValueHistory[i];

                            if (_targetValueHistory[i] > maxVal)
                                maxVal = _targetValueHistory[i];
                        }

                        _deltaValue = DeltaValueDistribution.Evaluate(Mathf.Abs(maxVal - minVal));
                    }
                    else
                        _deltaValue = 0;


                    //float valueAverage = 0f;

                    //if (_targetValueHistory.Count > 0)
                    //{
                    //    for (int i = 0; i < _targetValueHistory.Count; i++)
                    //        valueAverage += _targetValueHistory[i];

                    //    valueAverage /= _targetValueHistory.Count;
                    //}

                    //_deltaValue = Mathf.Abs(_lastValuesAverage - valueAverage);
                    //_lastValuesAverage = valueAverage;

                    _deltaCurrentFrame = 0f;
                    _targetValueHistory.Clear();
                }
            }

            _currentValue = Mathf.MoveTowards(_currentValue, _targetValue, ((_targetValue > _currentValue ? ValueAttackVelocity : ValueReleaseVelocity)) * Time.deltaTime);

            OnUpdate();
        }


        public virtual void ApplyValue(float val)
        {
            _targetValue = val;
        }

        public virtual void OnUpdate()
        {

        }
    }
}