﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Vision.Settings;

namespace Vision.Game
{
    public class PlayerManager : MonoBehaviour
    {
        public TMPro.TMP_Text HighscoreDisplay;

        public GameObject PlayerPrefab;
        [Space]
        public Transform SpawnCenter;

        private readonly Dictionary<string, Player> _players = new Dictionary<string, Player>();
        private readonly List<HighscoreEntry> _highschore = new List<HighscoreEntry>();

        private bool _updateUI = true;


        public void PurgePlayer(string username, string reason)
        {
            Dictionary<string, Player> copyPlayers = new Dictionary<string, Player>(_players);

            foreach (var pair in copyPlayers)
            {
                if (pair.Value.PlayerData.PlayerName.Equals(username, System.StringComparison.OrdinalIgnoreCase))
                {
                    Debug.Log($"Purged player matching '{username}' because of '{reason}'!");

                    LeaveGame(pair.Key);

                    for (int i = _highschore.Count - 1; i >= 0; i--)
                    {
                        if (_highschore[i].PlayerId == pair.Key)
                        {
                            _highschore.RemoveAt(i);
                            break;
                        }
                    }

                    _updateUI = true;
                }
            }
        }

        public void JoinGame(PlayerData data)
        {
            if (_players.Count < ConfigManager.Config.GameSettings.MaxPlayerCount && !_players.ContainsKey(data.PlayerId))
            {
                Player pl = SpawnPlayer(data);

                if (pl != null)
                    _players.Add(data.PlayerId, pl);
                else
                    Debug.LogError("Spawning Player failed!");
            }
        }
        public void LeaveGame(string userId)
        {
            if (!_players.ContainsKey(userId))
            {
                Debug.LogError($"Player ({userId}) not part of this round.");
                return;
            }

            if (_players.TryGetValue(userId, out Player player))
            {
                player.gameObject.SetActive(false);

                Destroy(player.gameObject, 3f);

                _players.Remove(userId);

                Debug.Log($"Player ({userId}) removed!");
            }
            else
                Debug.LogError($"Could not get Player ({userId}) from Dictionary!");
        }
        public void DropPlayer(string userId)
        {
            if (!_players.ContainsKey(userId))
            {
                Debug.LogError($"Player ({userId}) not part of this round.");
                return;
            }

            if (_players.TryGetValue(userId, out Player player))
                player.Drop();
            else
                Debug.LogError($"Could not get Player ({userId}) from Dictionary!");
        }

        public void OnMessageReceived(string userId, string username, string message, Color color)
        {
            if (ConfigManager.Config.GameSettings.AutoJoinOnMessage && !_players.ContainsKey(userId))
                JoinGame(new PlayerData(userId, username, color));

            if (_players.ContainsKey(userId))
                UpdatePlayer(_players[userId], message);
        }


        public PlayerData[] GetPlayerData()
        {
            List<PlayerData> players = new List<PlayerData>();

            foreach (var pair in _players)
                players.Add(pair.Value.PlayerData);

            return players.ToArray();
        }

        private Player SpawnPlayer(PlayerData data)
        {
            Player pl = Instantiate(PlayerPrefab, transform).GetComponent<Player>();
            pl.transform.position = SpawnCenter.transform.position + Vector3.up * _players.Count;

            pl.InitPlayer(this, data);

            return pl;
        }


        private void UpdatePlayer(Player player, string message)
        {
            player.Ability();
        }


        private void AddWinner(string playerId)
        {
            int entry = -1;

            for (int i = 0; i < _highschore.Count; i++)
            {
                if (_highschore[i].PlayerId != playerId)
                    continue;

                entry = i;
                break;
            }


            if (_players.TryGetValue(playerId, out Player pl))
            {
                if (entry < 0)
                {
                    _highschore.Add(new HighscoreEntry(pl.PlayerData));
                    entry = _highschore.Count - 1;
                }

                _highschore[entry].Score++;
                _updateUI = true;
            }
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.parent.GetComponent<Player>() is Player p && p != null && p.WinnerDeclared == false)
            {
                p.WinnerDeclared = true;
                AddWinner(p.PlayerData.PlayerId);
            }
        }


        private void LateUpdate()
        {
            if (HighscoreDisplay == null || !ConfigManager.Config.UISettings.ShowScore)
                return;

            if (!_updateUI)
                return;

            _updateUI = false;

            StringBuilder str = new StringBuilder();

            _highschore.Sort((s1, s2) => s2.Score.CompareTo(s1.Score));

            for (int i = 0; i < _highschore.Count; i++)
                str.AppendLine($"<color=#{_highschore[i].PlayerColor}>{_highschore[i].PlayerName} - {_highschore[i].Score:00}</color>");

            HighscoreDisplay.text = str.ToString();
        }

        class HighscoreEntry
        {
            public int Score;
            public string PlayerName;
            public string PlayerId;
            public string PlayerColor;

            public HighscoreEntry(PlayerData data)
            {
                PlayerName = data.PlayerName;
                PlayerId = data.PlayerId;
                PlayerColor = ColorUtility.ToHtmlStringRGB(data.PlayerColor);
            }
        }
    }
}