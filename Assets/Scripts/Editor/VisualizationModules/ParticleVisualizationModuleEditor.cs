﻿using UnityEditor;
using Vision.VisualizationModules;

namespace Vision.Editor.VisualizationModules
{
    [CustomEditor(typeof(ParticleVisualizationModule))]
    [CanEditMultipleObjects]
    public class ParticleVisualizationModuleEditor : UnityEditor.Editor
    {
        SerializedProperty ParticleSystemComponentProperty;

        SerializedProperty ValueAttackProperty;
        SerializedProperty ValueReleaseProperty;

        SerializedProperty UseStopPlayProperty;
        SerializedProperty PlayThresholdProperty;

        SerializedProperty UseStartColorProperty;
        SerializedProperty StartColorGradientProperty;


        void OnEnable()
        {
            ParticleSystemComponentProperty = serializedObject.FindProperty(nameof(ParticleVisualizationModule.ParticleSystemComponent));

            ValueAttackProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueAttackVelocity));
            ValueReleaseProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueReleaseVelocity));

            UseStopPlayProperty = serializedObject.FindProperty(nameof(ParticleVisualizationModule.UseStopPlay));
            PlayThresholdProperty = serializedObject.FindProperty(nameof(ParticleVisualizationModule.PlayThreshold));

            UseStartColorProperty = serializedObject.FindProperty(nameof(ParticleVisualizationModule.UseStartColor));
            StartColorGradientProperty = serializedObject.FindProperty(nameof(ParticleVisualizationModule.StartColorGradient));
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(ParticleSystemComponentProperty);

            EditorGUILayout.PropertyField(ValueAttackProperty);
            EditorGUILayout.PropertyField(ValueReleaseProperty);

            EditorGUILayout.PropertyField(UseStopPlayProperty);
            using (new EditorGUI.DisabledGroupScope(!UseStopPlayProperty.boolValue))
            {
                EditorGUILayout.PropertyField(PlayThresholdProperty);
            }

            EditorGUILayout.PropertyField(UseStartColorProperty);
            using (new EditorGUI.DisabledGroupScope(!UseStartColorProperty.boolValue))
            {
                EditorGUILayout.PropertyField(StartColorGradientProperty);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}