using UnityEngine;

namespace Vision.VisualizationModules
{
    public class CameraVisualizationModule : VisualizationModule
    {
        [Header("General")]
        public Camera CameraComponent;

        [Header("Background Color")]
        public bool UseBackgroundColorRotator;
        public Gradient BackgroundColorGradient;
        public AnimationCurve RotationSpeed = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(1f, 1f) } );
    
        private float _currentBackgroundColorTime;

        public override void OnUpdate()
        {
            if (CameraComponent == null)
                return;

            base.OnUpdate();

            if (UseBackgroundColorRotator && CameraComponent.clearFlags == CameraClearFlags.SolidColor)
            {
                _currentBackgroundColorTime += (RotationSpeed.Evaluate(_deltaValue) * Time.deltaTime);

                if (_currentBackgroundColorTime > BackgroundColorGradient.colorKeys[BackgroundColorGradient.colorKeys.Length-1].time)
                    _currentBackgroundColorTime = 0;

                CameraComponent.backgroundColor = BackgroundColorGradient.Evaluate(_currentBackgroundColorTime);
            }

        }
    }
}