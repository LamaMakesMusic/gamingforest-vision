﻿using UnityEngine;

namespace Vision.VisualizationModules
{
    public class DebugVisualizationModule : TransformVisualizationModule
    {

        [Header("Debug Texts")]
        public TextMesh AreaDisplay;
        public TextMesh StrengthDisplay;

        private Renderer _renderer;
        private MaterialPropertyBlock _materialPropertyBlock;
        private int _colorId;

        private void Awake()
        {
            _renderer = TransformComponents[0].GetComponentInChildren<Renderer>();
            _materialPropertyBlock = new MaterialPropertyBlock();

            _colorId = Shader.PropertyToID("_Color");
        }

        public void UpdateArea(int areaIndex, Color areaColor)
        {
            if (_renderer == default || _materialPropertyBlock == default)
                return;

            AreaDisplay.text = areaIndex.ToString();

            _renderer.GetPropertyBlock(_materialPropertyBlock);

            _materialPropertyBlock.SetColor(_colorId, areaColor);

            _renderer.SetPropertyBlock(_materialPropertyBlock);
        }

        public override void ApplyValue(float val)
        {
            base.ApplyValue(val);

            val = Mathf.RoundToInt(val * 100f) * 0.01f;

            if (StrengthDisplay != null)
                StrengthDisplay.text = val.ToString("F2");
        }

        public override void OnUpdate()
        {
            base.OnUpdate();
        }
    }
}