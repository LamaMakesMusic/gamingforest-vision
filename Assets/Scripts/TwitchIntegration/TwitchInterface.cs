﻿using System.Threading.Tasks;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using TwitchLib.Communication.Events;
using TwitchLib.Unity;
using UnityEngine;
using Vision.Game;
using Vision.Settings;

namespace Vision.TwitchIntegration
{
    public class TwitchInterface : MonoBehaviour
	{
	#if UNITY_EDITOR
		[Header("Auto Fire")]
		public Gradient AutoMessageColours;

		[Header("Debug Settings")]
		public bool CreateDebugMessages = false;
		[Range(.1f, 300f)]
		public float DebugMessageEveryXSeconds = 2f;
		private float debugTimer = 0f;
		private readonly string[] debugUser = { "Günther", "Fred", "Malte", "Anna", "Sophia", "Melanie", "Wolfgang", "Margareth", "DerDestroyerMitDenLangenNamen", "Kit", "Monte" };
		private readonly string[] debugMsgs = { "Boom", "Hallo!", "Bin ich im Fernsehen?!", "Ist das dieses Internet von dem alle Sprechen?! :o", "Grüße gehen raus!1!", "WOOP WOOP WOoP!", ":)", "<3 <3", "<555555", "Liebe Grüße an alle die das lesen!", "* * * * * * * *", "GebenSieIhrPasswortHierEin", "-.- Cool.", "MIAU MIAU MIAU" };
	#endif
	
		private Client _client;
		public PlayerManager PlayerManager { get; set; }

		private bool _boundCallbacks = false;
		private Task _connectTask;


        private void OnEnable()
        {
			TwitchAuthentication.OnLog += Log;
        }

        private void OnDisable()
        {
            TwitchAuthentication.OnLog -= Log;
        }

        private static void Log(object source, LogEventArgs args)
		{
			string line = $"[{args.Source}] {args.Message}";
			
			if (args.Exception != null)
				Debug.LogException(args.Exception);

            switch (args.Severity)
            {
                case LogEventArgs.LogSeverity.Critical:
                case LogEventArgs.LogSeverity.Error:
					Debug.LogError(line);
                    break;

                case LogEventArgs.LogSeverity.Warning:
					Debug.LogWarning(line);
                    break;

                case LogEventArgs.LogSeverity.Info:
                case LogEventArgs.LogSeverity.Verbose:
                case LogEventArgs.LogSeverity.Debug:
					Debug.Log(line);
                    break;
            }
        }


        private void Awake()
        {
			_client = new Client();
        }


        public void Initialize()
		{
			if (_client.IsConnected == ConfigManager.Config.TwitchCredentials.ConnectToTwitchChat)
				return;

            BindCallbacks();

			if (_client.IsConnected)
			{
                _client.Disconnect();
            }
            else if(string.IsNullOrWhiteSpace(ConfigManager.Config.TwitchCredentials.ChannelName))
			{
				Debug.LogError($"You need to enter a channel name in the {ConfigManager.CONFIG_FILE_NAME} if you want to listen to the chat!");
			}
			else if (_connectTask == null)
            {
				_connectTask = Task.Run(AuthenticateAndConnectToTwitch);
			}
			else
			{
				Debug.LogWarning("Already trying to connect to twitch!");
			}
		}

        private async Task AuthenticateAndConnectToTwitch()
        {
            ValidationResult result = await TwitchAuthentication.ValidateBotUserAuthentication("chat:read+chat:edit", ConfigManager.Config.TwitchCredentials.TwitchOAuthToken, ConfigManager.Config.TwitchCredentials.TwitchOAuthRefreshToken, ConfigManager.Config.TwitchCredentials.ClientId, ConfigManager.Config.TwitchCredentials.ClientSecret, ConfigManager.Config.TwitchCredentials.RedirectURL);

			if (!result.Successful)
				return;

			if (result.TokensUpdated)
            {
                ConfigManager.Config.TwitchCredentials.TwitchOAuthToken = result.OAuthToken;
                ConfigManager.Config.TwitchCredentials.TwitchOAuthRefreshToken = result.OAuthRefreshToken;
				ConfigManager.SaveConfig();
            }

            await ConnectToTwitch();
        }

        private async Task ConnectToTwitch()
		{
			//Create Credentials instance
			ConnectionCredentials credentials = new ConnectionCredentials(ConfigManager.Config.TwitchCredentials.TwitchUsername.ToLower(), ConfigManager.Config.TwitchCredentials.TwitchOAuthToken);

			// Create new instance of Chat Client
			if (_client.IsConnected)
			{
				_client.Disconnect();
				await Task.Delay(3000);
			}

			// Initialize the client with the credentials instance, and setting a default channel to connect to.
			_client.Initialize(credentials, ConfigManager.Config.TwitchCredentials.ChannelName, ConfigManager.Config.TwitchCredentials.CommandIdentifier[0], ConfigManager.Config.TwitchCredentials.CommandIdentifier[0]);

			// Connect
			_client.Connect();
		}

		private void BindCallbacks()
        {
			if (_boundCallbacks)
				return;

			_boundCallbacks = true;

            _client.OnLog += OnLog;
            _client.OnConnected += OnConnected;
            _client.OnDisconnected += OnDisconnected;
            _client.OnJoinedChannel += OnJoinedChannel;
			_client.OnLeftChannel += OnLeftChannel;
            _client.OnMessageReceived += OnMessageReceived;
            _client.OnChatCommandReceived += OnChatCommandReceived;
            _client.OnUserBanned += OnUserBanned;
			_client.OnUserTimedout += OnUserTimeout;
        }

        private void OnConnected(object sender, OnConnectedArgs e)
        {
			Debug.Log("Connected!");
        }

        private void OnDisconnected(object sender, OnDisconnectedEventArgs e)
        {
            Debug.Log("Disconnected!");
        }

        private void OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
			Debug.Log($"{e.BotUsername} joined channel '{e.Channel}'!");
        }

        private void OnLeftChannel(object sender, OnLeftChannelArgs e)
        {
            Debug.Log($"{e.BotUsername} left channel '{e.Channel}'!");
        }


        private void OnUserTimeout(object sender, OnUserTimedoutArgs e)
		{
			if (PlayerManager == null)
				return;

			PlayerManager.PurgePlayer(e.UserTimeout.Username, "timeout");
		}

		private void OnUserBanned(object sender, OnUserBannedArgs e)
		{
			if (PlayerManager == null)
				return;

			PlayerManager.PurgePlayer(e.UserBan.Username, "bann");
		}

		private void OnLog(object sender, OnLogArgs e)
		{
			Debug.Log(e.Data);
		}

		private void OnMessageReceived(object sender, OnMessageReceivedArgs e)
		{
			Debug.Log($"Message Received: {e.ChatMessage.Message}");

			if (ConfigManager.Config.GameSettings.OnlyCommandsTriggerUpdates == false)
			{
				ColorUtility.TryParseHtmlString(e.ChatMessage.ColorHex, out Color parsedColor);

				QueueCommand(e.ChatMessage.UserId, e.ChatMessage.DisplayName, e.ChatMessage.Message, parsedColor);
			}
		}

		private void OnChatCommandReceived(object sender, OnChatCommandReceivedArgs e)
		{
			if (PlayerManager == null)
				return;

			Debug.Log($"Command Received: {e.Command.ChatMessage.Message}");
		
			ColorUtility.TryParseHtmlString(e.Command.ChatMessage.ColorHex, out Color parsedColor);

			if (e.Command.CommandText == ConfigManager.Config.GameSettings.JoinGameCommand)
				PlayerManager.JoinGame(new PlayerData(e.Command.ChatMessage.UserId, e.Command.ChatMessage.DisplayName, parsedColor));
			else if (e.Command.CommandText == ConfigManager.Config.GameSettings.LeaveGameCommand)
				PlayerManager.LeaveGame(e.Command.ChatMessage.UserId);
			else if (e.Command.CommandText == ConfigManager.Config.GameSettings.DropGameCommand)
				PlayerManager.DropPlayer(e.Command.ChatMessage.UserId);
			else if (ConfigManager.Config.GameSettings.OnlyCommandsTriggerUpdates)
				QueueCommand(e.Command.ChatMessage.UserId, e.Command.ChatMessage.DisplayName, e.Command.ChatMessage.Message, parsedColor);
		}

		private void QueueCommand(string userId, string username, string message, Color color)
		{
			if (PlayerManager == null)
				return;

			message = message.Trim();
			if (message.StartsWith(ConfigManager.Config.TwitchCredentials.CommandIdentifier))
				message = message.Remove(0, 1);

			PlayerManager.OnMessageReceived(userId, username, message, color);
		}

	#if UNITY_EDITOR
		private void Update()
		{
			if (!CreateDebugMessages)
				return;

			debugTimer -= Time.deltaTime;
			if (debugTimer < 0f)
			{
				debugTimer = DebugMessageEveryXSeconds;
				string du = debugUser[Random.Range(0, debugUser.Length - 1)];
				QueueCommand(du, du, debugMsgs[Random.Range(0, debugMsgs.Length - 1)], AutoMessageColours.Evaluate(Mathf.Round(Random.Range(0f, 1f) * 10) * 0.1f));
			}
		}
#endif
    }
}