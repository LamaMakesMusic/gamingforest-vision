﻿using System;
using UnityEngine;

namespace Vision.VisualizationModules
{
    [Serializable]
    public class SpectrumArea
    {
        public string Name;

        public int AreaStart;
        public int AreaEnd;

        public VisualizationModule[] VisualizationModules;

        public void UpdateModuleValues(float[] dataSnippets)
        {
            if (dataSnippets == null || VisualizationModules == null)
                return;


            if (dataSnippets.Length == VisualizationModules.Length)
            {
                for (int i = 0; i < dataSnippets.Length; i++)
                    VisualizationModules[i]?.ApplyValue(dataSnippets[i]);
            }
            else if (dataSnippets.Length > VisualizationModules.Length)
            {
                int[] additionCount = new int[VisualizationModules.Length];
                float[] moduleValues = new float[VisualizationModules.Length];

                int moduleIndex = 0;
                for (int i = 0; i < dataSnippets.Length; i++)
                {
                    if (moduleIndex >= VisualizationModules.Length)
                        moduleIndex = 0;

                    moduleValues[moduleIndex] += dataSnippets[i];
                    additionCount[moduleIndex]++;

                    moduleIndex++;
                }


                for (int i = 0; i < moduleValues.Length; i++)
                    VisualizationModules[i]?.ApplyValue(moduleValues[i] / Mathf.Max(1, additionCount[i]));
            }
            else // if (dataSnippets.Length < VisualizationModules.Length)
            {
                int dataIndex = 0;

                for (int i = 0; i < VisualizationModules.Length; i++)
                {
                    if (dataIndex >= dataSnippets.Length)
                        dataIndex = 0;

                    VisualizationModules[i]?.ApplyValue(dataSnippets[dataIndex]);

                    dataIndex++;
                }
            }
        }

        public void ValidateAreaBoundaries()
        {
            if (AreaStart < 0)
                AreaStart = 0;

            AreaEnd = Mathf.Clamp(AreaEnd, AreaStart, 8192);
        }
    }
}