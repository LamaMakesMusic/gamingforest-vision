﻿namespace Vision.Settings
{
	public class Config
	{
		public UISettings UISettings = new UISettings();
		public AudioSettings AudioSettings = new AudioSettings();
		public GameSettings GameSettings = new GameSettings();
		public TwitchSettings TwitchCredentials = new TwitchSettings();

		public Config()
		{

		}
	}

	public class UISettings
	{
		public bool ShowDebugBars = false;
		public bool ShowCommands = true;
		public bool ShowScore = false;

		public UISettings()
		{

		}
	}

	public class AudioSettings
	{
		public float SpectrumCompressorStrength = 0.2f;
		public int MinFrequency = 50;
		public int MaxFrequency = 46000;
		public bool CaptureMicrophone = true;

		public AudioSettings()
		{

		}
	}

	public class GameSettings
	{
		public int TargetFramerate = 60;

		public string JoinGameCommand = "join";
		public string LeaveGameCommand = "leave";
		public string DropGameCommand = "drop";

		public int MaxPlayerCount = 20;

		public bool PreservePlayersOnSceneChange = true;

		public bool AutoJoinOnMessage = true;
		public bool OnlyCommandsTriggerUpdates = false;

		public float AbilityCooldownInSec = 3f;
		public float AbilityForce = 8f;

		public float DropCooldownInSec = 4f;
		public int DropHeight = 3;

		public GameSettings()
		{

		}
	}

	public class TwitchSettings
	{
		public string CommandIdentifier = "*";
		public bool ConnectToTwitchChat = false;

		public string ChannelName;
		public string TwitchUsername;
		public string TwitchOAuthToken;
		public string TwitchOAuthRefreshToken;
		public string ClientId;
		public string ClientSecret = "";
		public string RedirectURL = @"http://localhost:9001/";

        public TwitchSettings()
		{
			ChannelName = ""; //The channel you want to connect to.
			TwitchUsername = ""; // The name of the twitch account the oauth token is generated for.
            TwitchOAuthToken = ""; // The oauth token for your bot account.
			TwitchOAuthRefreshToken = ""; // The oauth refresh token for your bot account.
			ClientId = ""; // The clientId of the application (register at dev.twitch.tv)
            ClientSecret = ""; // The clientSecret of the application (DO NOT SHARE)
			RedirectURL = @"http://localhost:9001/";
		}
	}
}