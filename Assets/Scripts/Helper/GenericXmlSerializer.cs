﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public static class GenericXmlSerializer
{
    public static bool Exists(string filename, string directory)
    {
        filename = filename.Trim();
        if (!filename.EndsWith(".xml"))
            filename = $"{filename}.xml";

        string path = Path.Combine(directory, filename);

        bool result = File.Exists(path);

        if (result)
            Debug.Log($"Found file at: {path}");
        else
            Debug.LogWarning($"Could not locate file at: {path}");

        return result;
    }

    public static void SaveFile<T>(object objectToSave, string filename, string directoryPath)
    {
        try
        {
            T castedObject = (T)objectToSave;
            string path = Path.Combine(directoryPath, $"{Path.GetFileNameWithoutExtension(filename)}.xml");

            XmlSerializer writer = new XmlSerializer(typeof(T));
            FileStream file = File.Create(path);

            writer.Serialize(file, castedObject);

            file.Close();
        }
        catch (IOException e)
        {
            Debug.LogError($"Saving Failed!\n{e}");
        }
    }

    public static T LoadFile<T>(string filename, string directoryPath)
    {
        T loadedFile = default;

        try
        {
            string path = Path.Combine(directoryPath, $"{Path.GetFileNameWithoutExtension(filename)}.xml");

            XmlSerializer reader = new XmlSerializer(typeof(T));
            FileStream file = File.Open(path, FileMode.Open);
            object readFile = reader.Deserialize(file);
            file.Close();

            loadedFile = (T)readFile;

            Debug.Log($"Successfully loaded {filename}!");
        }
        catch (IOException e)
        {
            Debug.LogError($"Loading Failed!\n{e}");
        }

        return loadedFile;
    }
}

