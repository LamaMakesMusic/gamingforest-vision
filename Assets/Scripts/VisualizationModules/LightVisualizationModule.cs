﻿using UnityEngine;

namespace Vision.VisualizationModules
{
    public class LightVisualizationModule : VisualizationModule
    {
        [Header("General")]
        public Light[] LightComponents;

        [Header("Intensity")]
        public bool UseIntensity;
        public float MinimumIntensity = 0;
        public float MaximumIntensity = 1;

        [Header("Color")]
        public bool UseColor;
        public Gradient ColorGradient;


        public override void OnUpdate()
        {
            if (LightComponents == null || LightComponents.Length == 0)
                return;

            base.OnUpdate();

            for (int i = 0; i < LightComponents.Length; i++)
            {
                if (UseIntensity)
                    LightComponents[i].intensity = Mathf.Lerp(MinimumIntensity, MaximumIntensity, _currentValue);

                if (UseColor)
                    LightComponents[i].color = ColorGradient.Evaluate(_currentValue);
            }

        }
    }
}