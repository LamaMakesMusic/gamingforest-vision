﻿using UnityEditor;
using Vision.VisualizationModules;

namespace Vision.Editor.VisualizationModules
{
    [CustomEditor(typeof(MaterialVisualizationModule))]
    [CanEditMultipleObjects]
    public class MaterialVisualizationModuleEditor : UnityEditor.Editor
    {
        SerializedProperty ValueAttackProperty;
        SerializedProperty ValueReleaseProperty;

        SerializedProperty RendererComponentProperty;
        SerializedProperty TextMeshComponentProperty;

        SerializedProperty UseColorProperty;
        SerializedProperty ColorGradientProperty;


        void OnEnable()
        {
            ValueAttackProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueAttackVelocity));
            ValueReleaseProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueReleaseVelocity));

            RendererComponentProperty = serializedObject.FindProperty(nameof(MaterialVisualizationModule.RendererComponent));
            TextMeshComponentProperty = serializedObject.FindProperty(nameof(MaterialVisualizationModule.TextMeshComponent));

            UseColorProperty = serializedObject.FindProperty(nameof(MaterialVisualizationModule.UseColor));
            ColorGradientProperty = serializedObject.FindProperty(nameof(MaterialVisualizationModule.ColorGradient));
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(ValueAttackProperty);
            EditorGUILayout.PropertyField(ValueReleaseProperty);

            EditorGUILayout.PropertyField(RendererComponentProperty);
            EditorGUILayout.PropertyField(TextMeshComponentProperty);

            if (RendererComponentProperty.arraySize == 0 && TextMeshComponentProperty.arraySize == 0)
                EditorGUILayout.HelpBox("At least one component must be referenced! Script will not work!", MessageType.Warning);

            EditorGUILayout.PropertyField(UseColorProperty);
            using (new EditorGUI.DisabledGroupScope(!UseColorProperty.boolValue))
            {
                EditorGUILayout.PropertyField(ColorGradientProperty);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}