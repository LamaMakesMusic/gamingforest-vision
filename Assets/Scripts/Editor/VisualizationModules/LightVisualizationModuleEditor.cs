﻿using UnityEditor;
using Vision.VisualizationModules;

namespace Vision.Editor.VisualizationModules
{
    [CustomEditor(typeof(LightVisualizationModule))]
    [CanEditMultipleObjects]
    public class LightVisualizationModuleEditor : UnityEditor.Editor
    {
        SerializedProperty ValueAttackProperty;
        SerializedProperty ValueReleaseProperty;

        SerializedProperty LightComponentProperty;

        SerializedProperty UseIntensityProperty;
        SerializedProperty IntensityMinimumProperty;
        SerializedProperty IntensityMaximumProperty;

        SerializedProperty UseColorProperty;
        SerializedProperty ColorGradientProperty;


        void OnEnable()
        {
            ValueAttackProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueAttackVelocity));
            ValueReleaseProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueReleaseVelocity));

            LightComponentProperty = serializedObject.FindProperty(nameof(LightVisualizationModule.LightComponents));

            UseIntensityProperty = serializedObject.FindProperty(nameof(LightVisualizationModule.UseIntensity));
            IntensityMinimumProperty = serializedObject.FindProperty(nameof(LightVisualizationModule.MinimumIntensity));
            IntensityMaximumProperty = serializedObject.FindProperty(nameof(LightVisualizationModule.MaximumIntensity));

            UseColorProperty = serializedObject.FindProperty(nameof(LightVisualizationModule.UseColor));
            ColorGradientProperty = serializedObject.FindProperty(nameof(LightVisualizationModule.ColorGradient));
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(ValueAttackProperty);
            EditorGUILayout.PropertyField(ValueReleaseProperty);

            EditorGUILayout.PropertyField(LightComponentProperty);

            if (LightComponentProperty.arraySize == 0)
                EditorGUILayout.HelpBox("Missing LightComponent Reference! Script will not work!", MessageType.Warning);

            EditorGUILayout.PropertyField(UseIntensityProperty);
            using (new EditorGUI.DisabledGroupScope(!UseIntensityProperty.boolValue))
            {
                EditorGUILayout.PropertyField(IntensityMinimumProperty);
                EditorGUILayout.PropertyField(IntensityMaximumProperty);
            }

            EditorGUILayout.PropertyField(UseColorProperty);
            using (new EditorGUI.DisabledGroupScope(!UseColorProperty.boolValue))
            {
                EditorGUILayout.PropertyField(ColorGradientProperty);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}