﻿namespace Vision.VisualizationModules
{
    public class VisualizationSplitter : VisualizationModule
    {
        public VisualizationModule[] UpdateReceiver;



        public override void ApplyValue(float val)
        {
            base.ApplyValue(val);

            for (int i = 0; i < UpdateReceiver.Length; i++)
                UpdateReceiver[i].ApplyValue(val);
        }
    }
}