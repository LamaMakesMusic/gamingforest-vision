﻿using System;
using System.IO;
using UnityEngine;

namespace Vision.Settings
{
    public static class ConfigManager
    {
        public static Config Config = new Config();

        private static string CONFIG_DIRECTORY;
        public const string CONFIG_FILE_NAME = "config.xml";

        public static void Initialize()
        {
            CONFIG_DIRECTORY = Path.Combine(Application.dataPath, "..");

            if (!GenericXmlSerializer.Exists(CONFIG_FILE_NAME, CONFIG_DIRECTORY))
                SaveConfig();
        }

        public static void SaveConfig()
        {
            try
            {
                GenericXmlSerializer.SaveFile<Config>(Config, CONFIG_FILE_NAME, CONFIG_DIRECTORY);

                Debug.Log("Saved Config File.");
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public static void LoadConfig()
        {
            try
            {
                Config = GenericXmlSerializer.LoadFile<Config>(CONFIG_FILE_NAME, CONFIG_DIRECTORY);

                Debug.Log("Loaded Config File.");
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}