﻿using Assets.WasapiAudio.Scripts.Core;
using Assets.WasapiAudio.Scripts.Unity;
using Assets.WasapiAudio.Scripts.Wasapi;
using System.Collections.Generic;
using UnityEngine;
using Vision.Settings;
using Vision.VisualizationModules;

namespace Vision
{
    public class AudioPrism : MonoBehaviour
    {
        public ModulesUpdater ModulesManager;

        [Header("Debug")]
        public Transform DebugPlane;
        public GameObject DebugBarPrefab;
        public Gradient DebugBarAreaColors;

        [Header("Prism Settings")]
        public int SpectrumResolution = 64;
        [Range(0f, 3f)]
        public float SpectrumCompressorStrength = 0f;

        [Header("WASAPI Settings")]
        public ScalingStrategy ScalingStrategy;
        public int MinFrequency = 0;
        public int MaxFrequency = 20000;
        public WasapiAudioFilter[] Filters;

        private WasapiAudio _wasapiAudioLoopback;
        private WasapiAudio _wasapiAudioMicrophone;
        private float[] _spectrumData;

        private float _compressorValue;

        private readonly List<DebugVisualizationModule> _debugBars = new List<DebugVisualizationModule>();

        private void OnEnable()
        {
            _spectrumData = new float[SpectrumResolution];


            _wasapiAudioLoopback = new WasapiAudio(WasapiCaptureType.Loopback, SpectrumResolution, ScalingStrategy, MinFrequency, MaxFrequency, Filters, spectrumData =>
            {
                _spectrumData = spectrumData;
            });
            _wasapiAudioLoopback.StartListen();


            if (ConfigManager.Config.AudioSettings.CaptureMicrophone)
            {
                _wasapiAudioMicrophone = new WasapiAudio(WasapiCaptureType.Microphone, SpectrumResolution, ScalingStrategy, MinFrequency, MaxFrequency, Filters, spectrumData =>
                {
                    for (int i = 0; i < spectrumData.Length; i++)
                        _spectrumData[i] = (_spectrumData[i] + spectrumData[i]) * .5f;
                });

                _wasapiAudioMicrophone.StartListen();
            }
        }
        private void OnDisable()
        {
            _wasapiAudioLoopback?.StopListen();
            _wasapiAudioMicrophone?.StopListen();
        }


        private void Update()
        {
            if (_spectrumData == null)
                return;

            _compressorValue = 1 + SpectrumCompressorStrength;
            if (_compressorValue > 1f)
            {
                for (int i = 0; i < _spectrumData.Length; i++)
                    _spectrumData[i] *= Mathf.Clamp(_compressorValue * (1f - _spectrumData[i]), 1f, 99f);
            }


            if (ModulesManager != null)
                ModulesManager.UpdateAreaValues(_spectrumData);

            UpdateDebugBars();
        }

        private void UpdateDebugBars()
        {
            EnsureDebugBars();

            for (int i = 0; i < _spectrumData.Length; i++)
            {
                if (_debugBars.Count > i)
                    _debugBars[i].ApplyValue(_spectrumData[i]);
            }
        }

        private void EnsureDebugBars()
        {
            if (_spectrumData.Length == _debugBars.Count)
                return;

            if (_spectrumData.Length > _debugBars.Count)
            {
                for (int i = 0; i < _spectrumData.Length - _debugBars.Count; i++)
                {
                    var go = Instantiate(DebugBarPrefab, DebugPlane);
                    _debugBars.Add(go.GetComponent<DebugVisualizationModule>());
                }
            }
            else
            {
                int count = _debugBars.Count - _spectrumData.Length;

                for (int i = 0; i < count; i++)
                {
                    Destroy(_debugBars[i], .1f);
                    _debugBars.RemoveAt(i);
                }
            }

            SortDebugBars();
        }

        private void SortDebugBars()
        {
            float rayPositionWidth = .05f;
            float startOffset = Mathf.RoundToInt(_spectrumData.Length * .5f) * rayPositionWidth;
            Vector3 position = -DebugPlane.transform.right.normalized * startOffset;

            for (int i = 0; i < _debugBars.Count; i++)
            {
                position += DebugPlane.transform.right.normalized * rayPositionWidth;

                _debugBars[i].transform.position = DebugPlane.position + position;

                _debugBars[i].UpdateArea(i, DebugBarAreaColors.Evaluate(i / (float)_debugBars.Count));
            }
        }

    }
}