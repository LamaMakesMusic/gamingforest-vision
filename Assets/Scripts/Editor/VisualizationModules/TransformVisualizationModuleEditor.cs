﻿using UnityEditor;
using Vision.VisualizationModules;

namespace Vision.Editor.VisualizationModules
{
    [CustomEditor(typeof(TransformVisualizationModule))]
    [CanEditMultipleObjects]
    public class TransformVisualizationModuleEditor : UnityEditor.Editor
    {
        SerializedProperty TransformComponentProperty;

        SerializedProperty ValueAttackProperty;
        SerializedProperty ValueReleaseProperty;

        SerializedProperty UsePositionProperty;
        SerializedProperty PositionMinimumProperty;
        SerializedProperty PositionMaximumProperty;

        SerializedProperty UseScaleProperty;
        SerializedProperty ScaleMinimumProperty;
        SerializedProperty ScaleMaximumProperty;


        void OnEnable()
        {
            TransformComponentProperty = serializedObject.FindProperty(nameof(TransformVisualizationModule.TransformComponents));

            ValueAttackProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueAttackVelocity));
            ValueReleaseProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueReleaseVelocity));

            UsePositionProperty = serializedObject.FindProperty(nameof(TransformVisualizationModule.UsePosition));
            PositionMinimumProperty = serializedObject.FindProperty(nameof(TransformVisualizationModule.MinimumPosition));
            PositionMaximumProperty = serializedObject.FindProperty(nameof(TransformVisualizationModule.MaximumPosition));

            UseScaleProperty = serializedObject.FindProperty(nameof(TransformVisualizationModule.UseScale));
            ScaleMinimumProperty = serializedObject.FindProperty(nameof(TransformVisualizationModule.MinimumScale));
            ScaleMaximumProperty = serializedObject.FindProperty(nameof(TransformVisualizationModule.MaximumScale));
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(TransformComponentProperty);

            EditorGUILayout.PropertyField(ValueAttackProperty);
            EditorGUILayout.PropertyField(ValueReleaseProperty);

            EditorGUILayout.PropertyField(UsePositionProperty);
            using (new EditorGUI.DisabledGroupScope(!UsePositionProperty.boolValue))
            {
                EditorGUILayout.PropertyField(PositionMinimumProperty);
                EditorGUILayout.PropertyField(PositionMaximumProperty);
            }

            EditorGUILayout.PropertyField(UseScaleProperty);
            using (new EditorGUI.DisabledGroupScope(!UseScaleProperty.boolValue))
            {
                EditorGUILayout.PropertyField(ScaleMinimumProperty);
                EditorGUILayout.PropertyField(ScaleMaximumProperty);
            }


            serializedObject.ApplyModifiedProperties();
        }
    }
}