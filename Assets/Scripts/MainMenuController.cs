using System.Net.Security;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vision;
using Vision.Game;
using Vision.Settings;
using Vision.TwitchIntegration;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController Instance;

    [Header("UI")]
    public GameObject MenuPanel;
    [Space]
    public GameObject MainButtonsPanel;
    public GameObject ScenesButtonsPanel;
    [Space]
    public Button ResumeButton;
    public Button ExitButton;
    public TMP_Text ExitButtonText;
    [Space]
    public GameObject SceneButtonPrefab;
    [Space]
    public TMP_Text CommandListDisplay;

    private TwitchInterface _twitchInterface;
    private AudioPrism _audioPrism;
    private PlayerManager _playerManager;

    private bool _isMainMenu = true;

    private Button[] _sceneButtons;
    private PlayerData[] _preservedPlayers = null;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            
            DontDestroyOnLoad(this.gameObject);

            _twitchInterface = GetComponent<TwitchInterface>();


            _sceneButtons = new Button[SceneManager.sceneCountInBuildSettings-1];
            for (int i = 1; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                Button button = Instantiate(SceneButtonPrefab, ScenesButtonsPanel.transform).GetComponent<Button>();
                button.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, -20 - (75 * i), 0);

                int sceneIndex = i;
                button.onClick.AddListener(() => SceneButton_Pressed(sceneIndex));

                string sceneName = SceneUtility.GetScenePathByBuildIndex(i);
                sceneName = System.IO.Path.GetFileNameWithoutExtension(sceneName);

                TMP_Text txt = button.GetComponentInChildren<TMP_Text>();
                txt.text = $"{i}. {sceneName}";

                _sceneButtons[i-1] = button;
            }


            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {
        if (Instance != this)
            return;

        MenuPanel.SetActive(true);
        MainButtonsPanel.SetActive(true);
        ScenesButtonsPanel.SetActive(false);

        ConfigManager.Initialize();

        ReloadConfig_Pressed();
    }


    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    private void Update()
    {
        if (_isMainMenu)
            return;

        if (Input.GetKeyDown(KeyCode.Escape))
            MenuPanel.SetActive(!MenuPanel.activeSelf);
    }


    public void OnSceneLoaded(Scene s, LoadSceneMode mode)
    {
        _isMainMenu = s.buildIndex == 0;

        MenuPanel.SetActive(_isMainMenu);

        ResumeButton.interactable = !_isMainMenu;

        ExitButton.onClick.RemoveAllListeners();
        ExitButton.onClick.AddListener(() => SceneButton_Pressed(_isMainMenu ? -1 : 0));
        ExitButtonText.text = _isMainMenu ? "Exit" : "To Menu";

        for (int i = 0; i < _sceneButtons.Length; i++)
            _sceneButtons[i].interactable = _isMainMenu || s.buildIndex != i+1;

        if (_isMainMenu)
            return;

        Debug.Log("Updating Scene References!");

        _playerManager = FindObjectOfType<PlayerManager>();
        _twitchInterface.PlayerManager = _playerManager;
        _audioPrism = FindObjectOfType<AudioPrism>();

        ReloadConfig_Pressed();
        
        if (_preservedPlayers != null)
        {
            for (int i = 0; i < _preservedPlayers.Length; i++)
                _playerManager.JoinGame(_preservedPlayers[i]);

            _preservedPlayers = null;
        }
    }


    public void ReloadConfig_Pressed()
    {
        ConfigManager.LoadConfig();

        Application.targetFrameRate = ConfigManager.Config.GameSettings.TargetFramerate != 0 ? ConfigManager.Config.GameSettings.TargetFramerate : -1;

        if (_audioPrism != null)
        {
            _audioPrism.enabled = false;

            _audioPrism.SpectrumCompressorStrength = ConfigManager.Config.AudioSettings.SpectrumCompressorStrength;
            _audioPrism.DebugPlane.gameObject.SetActive(ConfigManager.Config.UISettings.ShowDebugBars);
            
            _audioPrism.MinFrequency = ConfigManager.Config.AudioSettings.MinFrequency;
            _audioPrism.MaxFrequency = ConfigManager.Config.AudioSettings.MaxFrequency;

            _audioPrism.enabled = true;
        }


        if (ConfigManager.Config.UISettings.ShowCommands)
        {
            StringBuilder builder = new StringBuilder("<u>Commands:</u>\n");
            builder.AppendLine($"'{ConfigManager.Config.TwitchCredentials.CommandIdentifier}{ConfigManager.Config.GameSettings.JoinGameCommand}'");
            builder.AppendLine($"'{ConfigManager.Config.TwitchCredentials.CommandIdentifier}{ConfigManager.Config.GameSettings.LeaveGameCommand}'");
            builder.AppendLine($"'{ConfigManager.Config.TwitchCredentials.CommandIdentifier}{ConfigManager.Config.GameSettings.DropGameCommand}'");
            
            CommandListDisplay.text = builder.ToString();
        }

        CommandListDisplay.gameObject.SetActive(!_isMainMenu && ConfigManager.Config.UISettings.ShowCommands);
        
        if (_playerManager != null && _playerManager.HighscoreDisplay != null)
            _playerManager.HighscoreDisplay.gameObject.SetActive(ConfigManager.Config.UISettings.ShowScore);

        _twitchInterface?.Initialize();
    }

    public void SceneButton_Pressed(int sceneIndex)
    {
        if (sceneIndex != 0 && _playerManager != null)
        {
            if (ConfigManager.Config.GameSettings.PreservePlayersOnSceneChange)
                _preservedPlayers = _playerManager.GetPlayerData();
            else
                _preservedPlayers = null;
        }


        _playerManager = null;
        _twitchInterface.PlayerManager = null;
        _audioPrism = null;


        if (sceneIndex < 0)
            Application.Quit();
        else
            SceneManager.LoadScene(sceneIndex);

        ScenesButtonsPanel.SetActive(false);
        MainButtonsPanel.SetActive(true);
    }
}
