﻿using UnityEngine;
using Vision.Settings;

namespace Vision.Game
{
    public class Player : MonoBehaviour
    {
        public TMPro.TMP_Text NameTag;
        public Renderer[] ColoredRenderers;
        [Space]
        public GameObject[] FacePrefabs;
        [Space]
        public ParticleSystem AbilityParticles;

        private PlayerData _playerData;
        public PlayerData PlayerData { get { return _playerData; } }

        private long _abilityCooldownEnd;
        private long _dropCooldownEnd;

        public bool WinnerDeclared;

        private Rigidbody _rigidbody;
        public Rigidbody Rigidbody { get { return _rigidbody; } }

        private MaterialPropertyBlock _propBlock;
        private int _propNameId;

        private Vector3 _nameTagOffset;

        private GameObject _faceInstance;

        private Camera _cam;

        private PlayerManager _playerManager;


        private void Awake()
        {
            _rigidbody = GetComponentInChildren<Rigidbody>();
            _rigidbody.sleepThreshold = 0;

            _cam = Camera.main;

            if (NameTag != null)
                _nameTagOffset = NameTag.transform.localPosition;

            _propNameId = Shader.PropertyToID("_Color");
            _propBlock = new MaterialPropertyBlock();
        }


        private void Update()
        {
            if (_rigidbody.transform.position.y < -50f)
            {
                _rigidbody.velocity = Vector3.zero;
                _rigidbody.transform.localPosition = Vector3.zero;

                _playerManager?.LeaveGame(_playerData.PlayerId);
            }
        }

        private void LateUpdate()
        {
            if (NameTag != null)
            {
                NameTag.transform.position = _rigidbody.transform.position + _nameTagOffset;
                NameTag.transform.forward = (NameTag.transform.position - _cam.transform.position);
            }
        }


        public void InitPlayer(PlayerManager context, PlayerData playerData)
        {
            _playerManager = context;

            _rigidbody.velocity = Vector3.zero;
            _rigidbody.transform.localPosition = Vector3.zero;

            _playerData = playerData;

            WinnerDeclared = false;

            if (NameTag != null)
            {
                NameTag.text = playerData.PlayerName;
                NameTag.color = playerData.PlayerColor;
            }

            for (int i = 0; i < ColoredRenderers.Length; i++)
            {
                ColoredRenderers[i].GetPropertyBlock(_propBlock);
                {
                    _propBlock.Clear();
                    _propBlock.SetColor(_propNameId, playerData.PlayerColor);
                }
                ColoredRenderers[i].SetPropertyBlock(_propBlock);
            }

            if (_faceInstance)
                Destroy(_faceInstance);

            if (FacePrefabs.Length > 0)
            {
                _faceInstance = Instantiate(FacePrefabs[Random.Range(0, FacePrefabs.Length)], _rigidbody.transform);
                _faceInstance.transform.localPosition = Vector3.zero;    
            }
        }


        public void Ability()
        {
            if (_abilityCooldownEnd > System.DateTime.Now.Ticks)
            {
                Debug.Log($"<color=yellow>{_playerData.PlayerName}: Ability cooldown still active!</color>");
                return;
            }
        
            _abilityCooldownEnd = System.DateTime.Now.Ticks + System.TimeSpan.FromSeconds(ConfigManager.Config.GameSettings.AbilityCooldownInSec).Ticks;

            var direction = _rigidbody.velocity.normalized;
            direction.y = .666f;

            _rigidbody.AddForce(direction * ConfigManager.Config.GameSettings.AbilityForce, ForceMode.Impulse);

            AbilityParticles.Play();

            Debug.Log($"<color=green>{_playerData.PlayerName}: Ability activated!</color>");
        }
        public void Drop()
        {
            if (_dropCooldownEnd > System.DateTime.Now.Ticks)
            {
                Debug.Log($"<color=yellow>{_playerData.PlayerName}: Drop cooldown still active!</color>");
                return;
            }

            _dropCooldownEnd = System.DateTime.Now.Ticks + System.TimeSpan.FromSeconds(ConfigManager.Config.GameSettings.DropCooldownInSec).Ticks;


            _rigidbody.transform.localPosition += Vector3.up * ConfigManager.Config.GameSettings.DropHeight;
        }
    }

    public struct PlayerData
    {
        private string _playerName;
        public string PlayerName { get { return _playerName; } }

        private string _playerId;
        public string PlayerId { get { return _playerId; } }

        private Color _playerColor;
        public Color PlayerColor { get { return _playerColor; } }

        public PlayerData(string playerId, string playerName, Color playerColor)
        {
            _playerName = playerName;
            _playerId = playerId;
            _playerColor = playerColor;
        }
    }
}