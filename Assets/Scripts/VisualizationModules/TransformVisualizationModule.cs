﻿using UnityEngine;

namespace Vision.VisualizationModules
{
    public class TransformVisualizationModule : VisualizationModule
    {
        [Header("General")]
        public Transform[] TransformComponents;

        [Header("Position")]
        public bool UsePosition;
        public Vector3 MinimumPosition = Vector3.zero;
        public Vector3 MaximumPosition = Vector3.zero;

        [Header("Scale")]
        public bool UseScale;
        public Vector3 MinimumScale = Vector3.one;
        public Vector3 MaximumScale = Vector3.one;

        public override void OnUpdate()
        {
            if (TransformComponents == null || TransformComponents.Length == 0)
                return;

            base.OnUpdate();

            for (int i = 0; i < TransformComponents.Length; i++)
            {
                if (UsePosition)
                    TransformComponents[i].localPosition = Vector3.Lerp(MinimumPosition, MaximumPosition, _currentValue);

                if (UseScale)
                    TransformComponents[i].localScale = Vector3.Lerp(MinimumScale, MaximumScale, _currentValue);
            }
        }
    }
}