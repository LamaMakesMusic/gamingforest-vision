﻿using System.Collections.Generic;
using UnityEngine;
using Vision;

namespace Vision.VisualizationModules
{
    public class ModulesUpdater : MonoBehaviour
    {
        public AudioPrism AudioPrism;
        public SpectrumArea[] AreaModules;


        private void Start()
        {
            if (AudioPrism == null)
            {
                Debug.LogError($"{nameof(AudioPrism)} not found!", gameObject);
                return;
            }

            for (int i = 0; i < AreaModules?.Length; i++)
                AreaModules[i].ValidateAreaBoundaries();
        }

        public void UpdateAreaValues(float[] data)
        {
            if (data == null || AreaModules == null)
                return;

            List<float> dataAreaSnippet = new List<float>();

            for (int i = 0; i < AreaModules.Length; i++)
            {
                if (data.Length <= AreaModules[i].AreaStart)
                {
                    Debug.LogWarning($"Data Length <= AreaStart ({data.Length} <= {AreaModules[i].AreaStart})");
                    continue;
                }

                int areaRange = AreaModules[i].AreaEnd - AreaModules[i].AreaStart;
                if (areaRange < 1)
                    areaRange = 1;

                for (int j = 0; j < areaRange; j++)
                {
                    int areaDataIndex = AreaModules[i].AreaStart + j;

                    if (areaDataIndex < data.Length)
                        dataAreaSnippet.Add(data[areaDataIndex]);
                    else
                        break;
                }

                AreaModules[i].UpdateModuleValues(dataAreaSnippet.ToArray());

                dataAreaSnippet.Clear();
            }
        }

    }
}