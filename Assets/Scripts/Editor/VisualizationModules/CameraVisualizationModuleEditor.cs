using UnityEditor;
using Vision.VisualizationModules;

namespace Vision.Editor.VisualizationModules
{
    [CustomEditor(typeof(CameraVisualizationModule))]
    [CanEditMultipleObjects]
    public class CameraVisualizationModuleEditor : UnityEditor.Editor
    {
        SerializedProperty CameraComponentProperty;

        SerializedProperty ValueAttackProperty;
        SerializedProperty ValueReleaseProperty;

        SerializedProperty CreateDeltaValueProperty;
        SerializedProperty DeltaFrameProperty;
        SerializedProperty DeltaDistributionProperty;

        SerializedProperty UseBackgroundColorRotatorProperty;
        SerializedProperty BackgroundColorProperty;
        SerializedProperty RotationSpeedProperty;


        void OnEnable()
        {
            CameraComponentProperty = serializedObject.FindProperty(nameof(CameraVisualizationModule.CameraComponent));

            ValueAttackProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueAttackVelocity));
            ValueReleaseProperty = serializedObject.FindProperty(nameof(VisualizationModule.ValueReleaseVelocity));

            CreateDeltaValueProperty = serializedObject.FindProperty(nameof(VisualizationModule.CreateDeltaValue));
            DeltaFrameProperty = serializedObject.FindProperty(nameof(VisualizationModule.DeltaFrameInSeconds));
            DeltaDistributionProperty = serializedObject.FindProperty(nameof(VisualizationModule.DeltaValueDistribution));

            UseBackgroundColorRotatorProperty = serializedObject.FindProperty(nameof(CameraVisualizationModule.UseBackgroundColorRotator));
            BackgroundColorProperty = serializedObject.FindProperty(nameof(CameraVisualizationModule.BackgroundColorGradient));
            RotationSpeedProperty = serializedObject.FindProperty(nameof(CameraVisualizationModule.RotationSpeed));

        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(CameraComponentProperty);

            EditorGUILayout.PropertyField(ValueAttackProperty);
            EditorGUILayout.PropertyField(ValueReleaseProperty);

            using (new EditorGUI.DisabledGroupScope(true))
            {
                EditorGUILayout.PropertyField(CreateDeltaValueProperty);
                CreateDeltaValueProperty.boolValue = UseBackgroundColorRotatorProperty.boolValue;
            }
            EditorGUILayout.PropertyField(DeltaFrameProperty);
            EditorGUILayout.PropertyField(DeltaDistributionProperty);

            EditorGUILayout.PropertyField(UseBackgroundColorRotatorProperty);
            using (new EditorGUI.DisabledGroupScope(!UseBackgroundColorRotatorProperty.boolValue))
            {
                EditorGUILayout.PropertyField(BackgroundColorProperty);
                EditorGUILayout.PropertyField(RotationSpeedProperty);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}