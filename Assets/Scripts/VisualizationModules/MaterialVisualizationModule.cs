﻿using UnityEngine;

namespace Vision.VisualizationModules
{
    public class MaterialVisualizationModule : VisualizationModule
    {
        [Header("General")]
        public Renderer[] RendererComponent;
        public TextMesh[] TextMeshComponent;

        [Header("Color")]
        public bool UseColor;
        public Gradient ColorGradient;

        private MaterialPropertyBlock _propBlock;
        private int _colorId;
        private int _emissionColorId;

        private void OnEnable()
        {
            _colorId = Shader.PropertyToID("_Color");
            _emissionColorId = Shader.PropertyToID("_EmissionColor");
        }


        public override void OnUpdate()
        {
            base.OnUpdate();

            if (RendererComponent?.Length > 0)
            {
                for (int i = 0; i < RendererComponent.Length; i++)
                {
                    if (_propBlock == null)
                        _propBlock = new MaterialPropertyBlock();

                    RendererComponent[i].GetPropertyBlock(_propBlock);
                    {
                        if (UseColor)
                        {
                            _propBlock.SetColor(_colorId, ColorGradient.Evaluate(_currentValue));
                            _propBlock.SetColor(_emissionColorId, ColorGradient.Evaluate(_currentValue));
                        }
                    }
                    RendererComponent[i].SetPropertyBlock(_propBlock);
                }
            }

            if (TextMeshComponent?.Length > 0)
            {
                for (int i = 0; i < TextMeshComponent.Length; i++)
                {
                    if (UseColor)
                        TextMeshComponent[i].color = ColorGradient.Evaluate(_currentValue);
                }
            }
        }
    }
}